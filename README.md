# uniapp-build-cli

#### 介绍
这是一个uniapp发布android构建工具
主要用于解决 HBuilder X 反复云端打包时间长，本地打包环境配置反锁。

1.  此工具用nodejs编写，必须先安装nodejs
2.  用apksigner重新签名android包，必须先安装android开发环境
3.  如果HBuilder X中的项目基座发生变化，需要重新生成 apk母包

* 注意：此工具是对混合app (H5实现) 的Android包进行修改重组，重新签名得到新的apk，不仅只限于 HBuilder 项目使用。


#### 软件架构
1.  uniapp-build 工具主要实现原理：基于 HBuilder X 云端打包生成的apk 作为母包 与 生成本地打包App资源，进行组合重新签名apk。
2.  实现技术依赖于 android 开发环境，apksigner签名工具重新签名apk。


#### 安装教程

1.  安装 nodejs

2.  搭建android开发环境（推荐直接安装 Android Studio）

    添加 AndroidStudio/jre/bin 到windows环境变量

    检查 keytool 是否配置成功，查看版本 keytool -h （keytool 用于生成签名证书）


    配置apksigner签名工具（用于android包签名, 找到 C:\Users\mojon\AppData\Local\Android\Sdk\build-tools\31.0.0 添加到windows环境变量，请选择build-tools目录下最高版本）

    检查是否配置成功，查看版本 apksigner version

3.  安装 uniapp-build

    方式一、npm install uniapp-build --save-dev (安装到项目)

    方式二、npm install uniapp-build --glob (全局安装)

#### 使用说明

1.  HBuilder 发行 -> 原生app云打包生成apk文件，在目录./unpackage/release/apk下找到最新.apk文件复制一份（在前目录），改名为app.apk作为母包（用作 uniapp-build 的 apk母包）

2.  HBuilder 发行 -> 生成本地打包App资源, 在目录./unpackage/resources

4.  生成签名证书 keytool -genkey -alias app -keyalg RSA -validity 20000 -keystore app.keystore

3.  打包设置

方式一、uniapp-build 安装到项目

    项目目录下添加签名证书 app.keystore

    在项目 package.json 添加如下配置

    "scripts": {
        "apk": "uniapp-build build 生成本地打包App资源 app.apk母包 生成包目录 生成签名证书 证书密码"
    }

    打包 npm run pak


方式二、uniapp-build 全局安装

    检查是否安装成功，查看版本 uniapp-build -v

    打包 uniapp-build build 生成本地打包App资源 app.apk母包 生成包目录 生成签名证书 证书密码



###### 示例：
``` 
uniapp-build build ./unpackage/resources/ ./unpackage/release/apk/app.apk ./unpackage/release.apk ./app.keystore password
``` 

#### 工具使用技术参考

1.  [安装nodejs](https://nodejs.org/en/) https://nodejs.org/en/

2.  [安装Android Studio](https://developer.android.google.cn/studio/) https://developer.android.google.cn/studio/

4.  [apksigner签名使用](https://developer.android.google.cn/studio/command-line/apksigner) https://developer.android.google.cn/studio/command-line/apksigner
 
5.  生成 .keystore 签名证书, 由 keytool 生成。具体使用此处不做说明


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
